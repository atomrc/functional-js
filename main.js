var tweets = require("./tweets");

function map(fn) {

}

function pluck(path) {
    return function (object) {
        var attrs = path.split(".");

        return attrs.reduce(function (acc, attr) {
            return acc[attr];
        }, object);
    }
}

var countriesStats = tweets

    // on filter les comptes qui n'ont pas de localisation
    .filter(function hasLocation(tweet) {
        return tweet.twitter.user.location;
    })

    // on réduit le tableau de tweets en un objet qui contient 
    // les stats sur les pays
    .reduce(function statsOnCountries(countries, tweet) {
        var country = tweet.twitter.user.location;
        if (!countries[country]) {
            countries[country] = 0;
        }
        countries[country]++;
        return countries;
    });
console.log(
countriesStats
);

